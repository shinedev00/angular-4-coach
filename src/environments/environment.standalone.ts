import { Environment } from './environment.interface';

export const environment: Environment = {
  apiUrl: undefined,
  appName: 'ccr-web',
  cookieDomain: 'api.coachcaredev.com',
  loginSite: 'http://localhost:4200',
  production: false,
  role: 'provider',
  selveraApiEnv: 'test',
  url: 'http://localhost:4201'
};
