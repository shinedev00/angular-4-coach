import { Environment } from './environment.interface';

export const environment: Environment = {
  apiUrl: 'https://api.coachcaredev.com/',
  appName: 'ccr-web',
  cookieDomain: 'api.coachcaredev.com',
  loginSite: 'https://dashboard.coachcaredev.com',
  production: true,
  role: 'provider',
  selveraApiEnv: 'test',
  url: 'https://dashboard.coachcaredev.com/provider'
};
