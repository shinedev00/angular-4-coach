import { Environment } from './environment.interface';

export const environment: Environment = {
  apiUrl: 'https://api.coachcaredemo.com/',
  appName: 'ccr-web',
  cookieDomain: 'api.coachcaredemo.com',
  loginSite: 'https://dashboard.coachcaredemo.com',
  production: true,
  role: 'provider',
  selveraApiEnv: 'ccrDemo',
  url: 'https://dashboard.coachcaredemo.com/provider'
};
