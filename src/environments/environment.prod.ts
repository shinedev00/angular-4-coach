import { Environment } from './environment.interface';

export const environment: Environment = {
  apiUrl: 'https://api.coachcare.com/',
  appName: 'ccr-web',
  cookieDomain: 'api.coachcare.com',
  loginSite: 'https://dashboard.coachcare.com',
  production: true,
  role: 'provider',
  selveraApiEnv: 'prod',
  url: 'https://dashboard.coachcare.com/provider'
};
