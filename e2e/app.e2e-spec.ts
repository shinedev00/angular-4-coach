import { CcrStaticProviderPage } from './app.po';

describe('ccr-static-provider App', () => {
  let page: CcrStaticProviderPage;

  beforeEach(() => {
    page = new CcrStaticProviderPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
