import { Environment } from './environment.interface';

export const environment: Environment = {
  apiUrl: undefined,
  appName: 'ccr-web',
  cookieDomain: 'api.coachcare.loc',
  loginSite: 'https://dashboard.coachcare.loc',
  production: false,
  role: 'provider',
  selveraApiEnv: 'dev',
  url: 'https://dashboard.coachcare.loc/provider'
};
